Analysis of data on the Aboriginal participation in Saskatchewan economy from the Statistics Canada’s socioeconomic data repository (previously known as CANSIM).

By Petr Baranovskiy for the Northwest Saskatchewan Policy Unit, Johnson-Shoyama Graduate School of Public Policy, University of Saskatchewan.

Referrences: 

Natural Resources Canada. 2018. Indigenous Mining Agreements, Lands and Minerals Sector. Accessed October 20, 2019. http://atlas.gc.ca/imaema/en/.

Pebesma, Edzer. 2019. lwgeom: Bindings to Selected 'liblwgeom' Functions for Simple Features. R package version 0.1-7. https://CRAN.R-project.org/package=lwgeom.

_________. 2018. Simple Features for R: Standardized Support for Spatial Vector Data. The R Journal 10 (1), 439-446, https://doi.org/10.32614/RJ-2018-009.

Pebesma E., T. Mailund, and J. Hiebert. 2016. “Measurement Units in R.” R Journal, 8 (2), 486-494. doi: 10.32614/RJ-2016-061. https://doi.org/10.32614/RJ-2016-061.

Shkolnik, Dmitry. 2019. cansim: Accessing Statistics Canada Data Table and Vectors. R package version 0.3.2. https://CRAN.R-project.org/package=cansim.

Slowikowski, Kamil. 2019. ggrepel: Automatically Position Non-Overlapping Text Labels with 'ggplot2'. R package version 0.8.1. https://CRAN.R-project.org/package=ggrepel.

Statistics Canada. 2011 Census Boundary files. Provinces/territories Cartographic Boundary File. http://www12.statcan.gc.ca/census-recensement/2011/geo/bound-limit/files-fichiers/gpr_000b11a_e.zip.

_________. 2011. “2011 Census Boundary files. Water File - Lakes and rivers (polygons)." Accessed December 18, 2018. http://www12.statcan.gc.ca/census-recensement/2011/geo/bound-limit/files-fichiers/ghy_000c11a_e.zip.

_________. 2015a. Table 41-10-0026 Aboriginal peoples survey, harvesting activities by Aboriginal identity, age group and sex, population aged 15 years and over, Canada, provinces and territories. Statistics Canada Data (database). Using cansim package for the R language. Release date November 9, 2015.

_________. 2015b. Table 41-10-0028 Aboriginal peoples survey, reasons for harvesting, by Aboriginal identity, age group and sex, population aged 15 years and over, Canada, provinces and territories. Using cansim package for the R language. Release date November 9, 2015.

_________. 2015c. Table 41-10-0022 Aboriginal peoples survey, making handcrafted goods by Aboriginal identity, age group and sex, population aged 15 years and over, Canada, provinces and territories. Using cansim package for the R language. Release date November 9, 2015.

_________. 2015d. Table 41-10-0024 Aboriginal peoples survey, reasons for making handcrafted goods, by Aboriginal identity, age group and sex, population aged 15 years and over, Canada, provinces and territories. Using cansim package for the R language. Release date November 9, 2015.

_________. 2018a. Table 41-10-0014 Reasons for difficulty in finding work by Aboriginal identity, unemployed. Statistics Canada Data (database). Using cansim package for the R language. Release date December 5, 2018.

_________. 2018b. Table 41-10-0016 What would help most to find a job by Aboriginal identity, unemployed. Statistics Canada Data (database). Using cansim package for the R language. Release date December 5, 2018.

_________. 2018c. Table 37-10-0099 Distribution of the population aged 25 to 64 (total and with Aboriginal identity), by highest certificate, diploma or degree and age group. Statistics Canada Data (database). Using cansim package for the R language. Release date Release date March 21, 2018.

_________. 2018d. Table 37-10-0100 Distribution of population aged 25 to 64 (total and with Aboriginal identity), by sex and educational attainment. Statistics Canada Data (database). Using cansim package for the R language. Release date Release date March 21, 2018.

_________. 2018e. Table 37-10-0117 Educational attainment in the population aged 25 to 64, off-reserve Aboriginal, non-Aboriginal and total population. Statistics Canada Data (database). Using cansim package for the R language. Release date Release date September 29, 2018.

_________. 2019a. Table 14-10-0364 Labour Force Characteristics by Province, Region and Aboriginal Group. Statistics Canada Data (database). Using cansim package for the R language. Release date April 1, 2019.

_________. 2019b. Table 14-10-0370 Average hourly and weekly wages and average usual weekly hours by Aboriginal group. Statistics Canada Data (database). Using cansim package for the R language. Release date April 1, 2019.

Tennekes, Martijn. 2018. “tmap: Thematic Maps in R.” Journal of Statistical Software 84 (6). doi:10.18637/jss.v084.i06. https://doi.org/10.18637/jss.v084.i06.

von Bergmann, J., Dmitry Shkolnik, and Aaron Jacobs (2019). cancensus: R package to access, retrieve, and work with Canadian Census data and geography. R package version 0.2.0. https://mountainmath.github.io/cancensus/.

Wenseleers, Tom, and Christophe Vanderaa. 2018. “export: Streamlined Export of Graphs and Data Tables.” R package version 0.2.2. https://CRAN.R-project.org/package=export.

Wickham, Hadley. 2017. “tidyverse: Easily Install and Load the ‘Tidyverse.’” R package version 1.2.1. https://cran.r-project.org/package=tidyverse.
